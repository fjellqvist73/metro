import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
//import Button from './Button';
import registerServiceWorker from './registerServiceWorker';
//import { BrowserRouter } from 'react-router-dom'

console.log('Denna logger i filen index.js');
ReactDOM.render(<App />, document.getElementById('root'));
/*ReactDOM.render((
  <BrowserRouter>
    <App />
  </BrowserRouter>
), document.getElementById('root'));*/
registerServiceWorker();
