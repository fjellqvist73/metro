import React, { Component } from 'react';

export class SeachBox extends Component {
  state = {
    jobsearch: ''
  }

  change = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.state);
  }

  render() {
    return(
      <div class="offset-3 col-6 seach-form">
        <form action="/" method="get">
          Sök jobb:<br/>
          <input 
            name='jobsearch'
            placeholder='Sökord' 
            value={this.state.jobsearch}
            onChange={e => this.change(e)}
          />
          <br/>
          
          <br/>
          <button onClick={e => this.onSubmit(e)}>Sök</button>
        </form> 
      </div>
    );
  }
}

export default SeachBox;