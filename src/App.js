import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import SearchBox from './SearchBox';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      searchText: null
    };
  }

  jobList = (url) => {
    fetch(url)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  componentDidMount() {
    let  url = 'http://www.metrojobb.se/ws/search/searchjobad';
    this.jobList(url);
  }

  componentDidUpdate(prevProps, prevState) {
    let currentState = this.state.searchText;
    if (currentState !== null) {
      if (this.state.searchText !== prevState.searchText) {
        let baseUrl = "http://www.metrojobb.se/ws/search/searchjobad?query=";
        let  url = baseUrl.concat(currentState.jobsearch);
        this.jobList(url);
        }
      }
  }

  onSubmit = (fields) => {
    this.setState({
      searchText: fields
    })
  };

  render(props) {
    const { error, isLoaded, items } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      let jobs = Object.values(items.ads);
      let baseUrl = "http://www.metrojobb.se";

      const jobItems = jobs.map((job) =>
        <li>
          <img src={baseUrl.concat(job.logoUrl)} alt="logo"></img>
          <h5>{job.jobHeading}</h5>
          <p>{job.region3}</p>
          <a target="_blank" href={baseUrl.concat(job.linkUrl)}>Läs mer</a>
        </li>
      );

      return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <SearchBox {...props} onSubmit={fields => this.onSubmit(fields)}> )} </SearchBox>
          <ul class="jobList offset-3 col-6">
            {jobItems}
          </ul>
        </div>
      );
    }
  }
}

export default App;